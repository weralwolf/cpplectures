TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += main.cpp \
    Lecture_1/example_1.cpp \
    Lecture_2/lecture_2.cpp \
    Lecture_3/lecture_3.cpp \
    Lecture_4/List.cpp \
    Lecture_4/listTest.cpp \
    Lecture_5/Set.cpp \
    Lecture_5/setTest.cpp

HEADERS += \
    Lecture_1/lecture_1.h \
    Lecture_2/lecture_2.h \
    Lecture_3/lecture_3.h \
    Lecture_4/lecture_4.h \
    Lecture_5/Set.h \
    Lecture_5/lecture_5.h

