#ifndef __LECTURE_4_H__
#define __LECTURE_4_H__

#include <string>

namespace Lecture_4 {

int example();

class List {
public:
    struct LChunk {
        LChunk();
        LChunk(std::string _value);

        std::string value;
        LChunk * next;
        LChunk * prev;
    };

    List();
    List(List const & copy);
    ~List();

    void push(LChunk const & chunk);
    void push(std::string const & value);

    unsigned int size() const;

    LChunk pop();
    LChunk get(unsigned int index) const;

    void print() const;

    bool empty() const;

private:
    unsigned int amount;
    LChunk * first;
    LChunk * last;
};

};

#endif // __LECTURE_4_H__
