#include "lecture_4.h"
#include <iostream>

int Lecture_4::example() {
    List list;

    list.pop();
    list.pop();
    list.push("Hello tester!");
    list.pop();
    list.pop();
    list.push("Hello tester once more!");
    list.pop();

    list.print();
    for (unsigned int i = 0; i < 15; ++i) {
        std::string name = std::string("Mem_");
        name += 'a' + i;
        list.push(name);
    };
    list.print();
    List::LChunk pop = list.pop();
    std::cout << "Poped element: " << pop.value << ", size " << list.size()
              << std::endl;

    list.print();

    std::cout << "5th element: " << list.get(5).value << std::endl;

    return 0;
};
