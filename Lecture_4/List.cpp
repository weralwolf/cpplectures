#include "lecture_4.h"
#include <iostream>

using namespace Lecture_4;
//using std::cout; using std::endl;

List::LChunk::LChunk()
    :next(NULL), prev(NULL) {
};

List::LChunk::LChunk(std::string _value)
    :value(_value), next(NULL), prev(NULL) {
};

List::List()
    :first(NULL), last(NULL), amount(0) {
};

List::List(List const & copy) {
    for (LChunk * iterator = copy.first; iterator;
         iterator = iterator->next) {
        push(iterator->value);
    };
};

List::~List() {
    std::cout << "DELETE:" << std::endl;
    for (LChunk * iterator = first, * next = first->next;
         iterator;
         next = iterator->next) {
        std::cout << "delete " << iterator->value << std::endl;
        delete iterator;
        iterator = next;
    };
};

void List::push(LChunk const & chunk) {
    std::cout << "Converting..." << std::endl;
    push(chunk.value);
};

void List::push(std::string const & value) {
    std::cout << "Pushing value: " << value << std::endl;
    if (!amount) {
        last = new LChunk();
        last->prev = NULL;
        first = last;
    } else {
        last->next = new LChunk();
        last->next->prev = last;
        last = last->next;
    };
    last->next = NULL;
    last->value = value;
    ++amount;
};

unsigned int List::size() const {
    return amount;
};

List::LChunk List::pop() {
    if (amount) {
        LChunk result(last->value);
        LChunk * marker = last;

        if (amount == 1) {
            last = first = NULL;
            amount = 0;
        } else {
            last->prev->next = NULL;
            --amount;
        };

        delete marker;
        return result;
    };
    return LChunk();
};

List::LChunk List::get(unsigned int index) const {
    if (index < amount) {
        LChunk * iterator = first;
        for (unsigned i = 0;
             iterator && i < index;
             iterator = iterator->next, ++i);
        return LChunk(iterator->value);
    } else {
        return LChunk();
    };
};

void List::print() const {
    std::cout << "List { ";
    for (LChunk * iterator = first; iterator; iterator = iterator->next) {
        std::cout << iterator->value << " ";
    };
    std::cout << "};" << std::endl;
};

bool List::empty() const {
    return !amount;
};
