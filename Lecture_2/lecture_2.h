/* <LECTURE COMMENT #3>
 * Header file - файл заголовку, або як сленгово звично казати ’хедер’.
 * Файл, що вміщує в собі декларації усього що буде означено в cpp-файлі.
 * Під усим маються на увазі фунції та класи.
 *
 * Наступні два за коментарем рядокци та останній у файлі  так звані
 * declaration-guards, захистять нас від повторного означення тих самих функцій.
 * Як це працює і чому їх треба писати ми розберемось трошки пізніше, коли почнемо
 * говорити класами, а зараз запам’ятаємо, що їх має мати кожен файл заголовку.
 * Де ’__LECTURE_2_H__’ - унікальне на кожен такий файл ім’я.
 * В моїй традиції є наступне іменування:
 * __<HeaderFileName where all spaces replaced by _>_H__
 */
#ifndef __LECTURE_2_H__
#define __LECTURE_2_H__

/*
 * Найперше всі заголовки зовнішніх бібліотек, що приймають участь мають бути
 * підключені тут.
 */

#include <string>

/*
 * Далі слідують декларації усіх функцій. Вони лаконічно описують структуру.
 */
int example_2();

void first_function(int some_in);

int second_function(int some_other_in);

std::string third_function(std::string add_me);

void referenceCheck(int still, int * pointer, int & reference);

struct Point {
    int value;
    Point * next;
};

#endif // __LECTURE_2_H__
