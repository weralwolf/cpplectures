#include "lecture_5.h"
#include "Set.h"

#include <iostream>

int setTest() {
    UnorderedSet set;

    set = set + 1 + 2 + 3 + 4 + 1;

    std::cout << "Trying to add into our set 1, 2, 3, 4, 1." << std::endl
              << "How do you think, how many elements should it contains after?"
              << std::endl;
    std::cout << "Now size is: " << set.size() << std::endl;

    set = set - 1;

    std::cout << "Removing 1 from set." << std::endl;
    std::cout << "Now size is: " << set.size() << std::endl;

    set = set - 5;

    std::cout << "Removing 5 from set." << std::endl;
    std::cout << "Now size is: " << set.size() << std::endl;

    std::cout << "Set contains 6? " << (set.contains(6) ? "True" : "False")
              << std::endl;

    std::cout << "Set contains 3? " << (set.contains(3) ? "True" : "False")
              << std::endl;

    set = set - 3 - 4 - 2 - 2;

    std::cout << "Removing all from set." << std::endl;
    std::cout << "Now size is: " << set.size() << std::endl;

    int elements[5] = {4, 5, 1, 1, 0};
    UnorderedSet set2(elements, 5);

    std::cout << "Creation from array." << std::endl;
    std::cout << "Size is: " << set2.size() << std::endl;

    UnorderedSet set3(12, 0, 1, 3, 4, 5, 6, 3, 5, 5, 7, 4, 3);

    std::cout << "Creation by declaring elements." << std::endl;
    std::cout << "Size is: " << set3.size() << std::endl;

    return 0;
};
