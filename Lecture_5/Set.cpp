#include "Set.h"

#include <stdarg.h>
#include <iostream>

UnorderedSet::Chunk::Chunk(int _value, Chunk * _next, Chunk * _prev)
    : value(_value), next(_next), prev(_prev) {
};

UnorderedSet::Chunk::Chunk(Chunk const & _value, Chunk * _next, Chunk * _prev)
    : value(_value.value), next(_next), prev(_prev) {
};

UnorderedSet::Chunk::Chunk(Chunk const & copy)
    : value(copy.value), next(0), prev(0) {
};

UnorderedSet::UnorderedSet()
    : count(0), first(0), last(0) {
};

UnorderedSet::UnorderedSet(int * begin, unsigned int size)
    : count(0), first(0), last(0) {
    for(unsigned int i = 0; i < size; insert(*(begin + i++)));
};

UnorderedSet::UnorderedSet(unsigned int size, ...)
    : count(0), first(0), last(0) {
    va_list values;
    va_start(values, size);
    for(unsigned int i = 0; i < size; ++i ) {
        insert(va_arg(values, int));
    };
    va_end(values);
};

UnorderedSet::UnorderedSet(UnorderedSet const & set)
    : count(0), first(0), last(0) {
    copy(set);
};

UnorderedSet::~UnorderedSet() {
    cleanUp();
};

UnorderedSet & UnorderedSet::operator=(UnorderedSet const & set) {
    if (&set == this) return *this;

    cleanUp();
    copy(set);
    return *this;
};

unsigned int UnorderedSet::size() const {
    return count;
};

bool UnorderedSet::contains(int element) const {
    return find(element);
};

UnorderedSet UnorderedSet::operator+(int element) const {
    UnorderedSet _set(*this);
    if (!_set.contains(element)) {
        _set.insert(element);
    };

    return _set;
};

UnorderedSet UnorderedSet::operator-(int element) const {
    UnorderedSet _set(*this);
    if (Chunk * soughtFor = _set.find(element)) {
        _set.remove(soughtFor);
    };

    return _set;
};

void UnorderedSet::insert(int element) {
    if (!count) {
        last = new Chunk(element);
        last->prev = 0;
        first = last;
    } else {
        last->next = new Chunk(element);
        last->next->prev = last;
        last = last->next;
    };
    last->next = 0;
    ++count;

};

void UnorderedSet::remove(Chunk * element) {
    if (element->next && element->prev) {
        // element somewhere inside list and list isn't empty
        element->prev->next = element->next;
        element->next->prev = element->prev;
    } else if (element->next && !element->prev) {
        // element in the very begining of list
        element->next->prev = element->prev;
        first = element->next;
    } else if (!element->next && element->prev) {
        // element in the very end of list
        last = element->prev;
        element->prev->next = element->next;
    } else {
        // the only element of list
        first = last = 0;
    };
    --count;
    delete element;
};

UnorderedSet::Chunk * UnorderedSet::find(int element) const {
    Chunk * soughtFor = first;
    for (; soughtFor && soughtFor->value != element; soughtFor = soughtFor->next);
    return soughtFor;
};

void UnorderedSet::cleanUp() {
    if (!count) return;
    for (Chunk * iterator = first, * next = first->next;; next = iterator->next) {
        delete iterator;
        iterator = next;

        if (!iterator) break;
    };
    first = last = 0;
    count = 0;
};

void UnorderedSet::copy(UnorderedSet const & set) {
    for(Chunk * iterator = set.first; iterator; iterator = iterator->next) {
        insert(iterator->value);
    };
};
