#ifndef __UNORDEREDSET_H__
#define __UNORDEREDSET_H__

class UnorderedSet {
    struct Chunk  {
        int value;
        Chunk * next, * prev;

        Chunk(int _value, Chunk * _next = 0, Chunk * _prev = 0);
        Chunk(Chunk const & _value, Chunk * _next = 0, Chunk * _prev = 0);
        Chunk(Chunk const & copy);
    };

public:
    UnorderedSet();
    UnorderedSet(int * begin, unsigned int size);
    UnorderedSet(unsigned int size, ...);
    UnorderedSet(UnorderedSet const & copy);
    ~UnorderedSet();

    UnorderedSet & operator=(UnorderedSet const & set);

    unsigned int size() const;
    bool contains(int element) const;

    UnorderedSet operator+(int element) const;
    UnorderedSet operator-(int element) const;

private:
    void insert(int element);
    void remove(Chunk * element);
    Chunk * find(int element) const;
    void cleanUp();
    void copy(UnorderedSet const & set);

private:
    unsigned int count;
    Chunk * first, * last;
};

#endif // __UNORDEREDSET_H__
