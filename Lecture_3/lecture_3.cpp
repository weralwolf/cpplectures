/* <LECTURE COMMENT #1>
 * Сьогодні однією з тем, котру ми розглянемо буде проблема запису та читання з
 * файлу. З цього святкового приводу ми підключимо особливу бібліотеку `fstream`
 * котра нам допоможе в цьому.
 */
#include<iostream>
#include<fstream>

/*
 * Директиви препроцесора. При кожному запуску компілятора спочатку запускається
 * препроцесор, який виконує директиви що починаються з символу `#`. При
 * виконанні цих команд створюється тимчасовий файл вихідного коду, з яким уже
 * працює компілятор.
 *
 * З однією з таких деректив ми вже давно знайомі та широко використовуємо, це
 * дерестива `#include`, за допомогою котрої включається файл іншої бібліотеки,
 * вона заміняється препроцесором на вміст вказанного файлу.
 *
 * Також дуже важливою э деректива `#define` суть котрої заключається у означенні
 * ідентифікатора, суть певної змінної, що є деякими керівним параметром для
 * нашого коду.
 */
#define CHAR_BUFFER_SIZE 255

/* <BUILD COMMENT>
 * Flag SEPARATE_COMPILATION just let you to use this file as separate program
 * which doesn't a piece of project. To do this compile it as usual but with
 * -DSEPARATE_COMPILATION aditional flag
 */
#ifndef SEPARATE_COMPILATION

#include "lecture_3.h"
int example_3() {
#else
int main() {
#endif
    /* <LECTURE COMMENT #2>
     * Для того, щоб щось записати в файл достатньо створити об’єкт файлового
     * потоку. Перш ніж ми почнемо з Вами працювати з потоками, давайте побудуємо
     * певну модель данного об’єкту. Що це таке? Гарною аналогією, корисною для
     * уяви були б водопровідні труби, що в займаються транспортуванням від
     * відправника до одержувача. Такі канали працюють в одному напрямку та є
     * дуже логічними та послідовними в своїх операціях. Одним з прикладів
     * потоків котрими ми користувались є `std::cin` & `std::cout`
     *
     * Нижче наведено два варіанти відкривання файлу для запису. Розглянемо для
     * початку закоментований варіант. Тут тип `std::ofstream` одразу вказує на
     * те, що це буде `output file stream` - тобто `вихідний файловий потік`, але
     * не плутайте `output` з `put out`, останнє в деяких випадках в англійській
     * мові має неоднозначні переклади.
     *
     * Отже, означивши змінну, як файловий потік для запису ми тепер можемо
     * з’єднати його з певним пунктом призначення, у нашому випадку - це файл на
     * жорсткому диску. Використовуючи метод `open` ми відкриваємо файл для запису.
     *
     * Чим відрізняється закоментований варіант від незакоментованого? Хм... Лише
     * тим місцем де ми вказуємо режим роботи з файлом. В закоментованому варіанті
     * надалі, який би ми не відкрили файл, ми зможемо його відкрити лише для
     * запису, у незакоментованому, для будь-якого файлу який ми відкриваємо ми
     * вказуємо режим роботи з ним. В нашому випадку цей режим - це
     * `std::ios_base::out`
     */
//    std::ofstream output_file_stream;
//    output_file_stream.open("Lecture_3/file.out");
    std::fstream output_file_stream;
    output_file_stream.open("Lecture_3/file.out", std::ios_base::out);

    /*
     * Те як ми зараз будемо працювати з файлами на запис чи читання вже є
     * знайомими для нас механізмами. Якщо гарно придивитись, то ми по суті лише
     * замінили `std::cout` чи `std::cin` на назву відповідного файлового об’єкту.
     */

    output_file_stream << "We've add something inside!" << std::endl;
    output_file_stream << "As well we can add some digits: " << 12 << " " << 4.6
                << std::endl;

    /*
     * Після завершення роботи з файловим потоком Ви маєте закрити його, тим самим
     * повністю впевнившись, що по завершенню програми у файлі буде саме, то що
     * Ви прохали. Причина невідповідності може бути в тому, що в якийсь момент
     * Вами з’ясується, що файловий потік, котрий виражається у вигляді об’єкту
     * має приховані механізми роботи в середині, котрі після закривання потоку
     * фіналізують усі операції та кінцево записують файл.
     */
    output_file_stream.close();

//    std::ifstream input_file_stream;
//    input_file_stream.open("Lecture_3/file.in");

    /*
     * При відкридті файлу в режимі читання, як `std::ios_base::in`, що в принципі
     * видко з рядочків під цим коментарем.
     */
    std::fstream input_file_stream;
    input_file_stream.open("Lecture_3/file.in", std::ios_base::in);

    /* <LECTURE COMMENT #3>
     * Тепер, ми власне і скористаємося тією константою, котру означили вище.
     * Зараз вона у нас виступатиме у ролі ідентифікатора довжини масиву.
     * Причому, помітимо, що на момент компіляції у всьому коді вже відбудеться
     * її заміна на те числове значення, що ми задали. А тому ми можемо нею
     * сміливо користуватись.
     */
    char buf[CHAR_BUFFER_SIZE];

    /*
     * Тут ми скористаємося функцією читання рядка. Тоді ж, що ми отримаємо після
     * її виклику у результаті? Хм... Це досить просте питання, але все ж:
     *
     * 0. Якщо довжина рядка більша ніж задана нами довжина буфера, то ми отримаємо
     *    CHAR_BUFFER_SIZE символів рядка, а саме перші CHAR_BUFFER_SIZE символів.
     *
     * 1. Якщо довжина рядка менша - то весь рядок і нічого зайвого.
     */
    input_file_stream.getline(buf, CHAR_BUFFER_SIZE);
    std::string input_string(buf);
    std::cout << "We've read something new: `" << input_string << std::endl;

    double v1;
    int v2, v3;

    input_file_stream >> v1 >> v2 >> v3;

    std::cout << "And other digits, so let's see: " << v1 << " " << v2 << " "
              << v3 << std::endl;

    input_file_stream.read(buf, CHAR_BUFFER_SIZE);

    std::string useless_text(buf);

    /** @todo: findout what function read data up to file end */
    std::cout << "Read thing: " << useless_text << std::endl;

    input_file_stream.close();

    /* <LECTURE COMMENT #4>
     * Тут нам доведеться познайомитись із поняттям структури. Що таке структура?
     * Нам з Вами (оскільки за певним рядом основних ознак ми люди) притаманно
     * об’єднувати певний набір об’єктів, властивостей, тощо під одним іменем.
     * Це стається через наше природнє бажання спрощувати мовлення та власне життя.
     * Одним з прикладів може слугавати зошит. Сам по собі зошит складається із
     * листочків, обкладинки, скріпок та всіляких додатків (не знаю, хто з Вас
     * пам’ятає ті часи, коли до зошиту додавались так звана "промокашка"), до тогож
     * всі ці елементи також можуть різнитися за власними властивостями: листки
     * можуть бути в клітинуи, лінію, косу лінію, пусті чи орнаментовані; обкладинка
     * мати якісь малюнки, різнитись матеріалом. Але факт залишиться фактом - це
     * все будуть ті чи інші зошити. Тому чому б не об’єднати певні властивості,
     * поля данних під єдиним гаслом чи назвою? Давайте так і зробимо (дивіться
     * об’явлення у файлі заголовку). Нижче наведено приклад заповнення данної
     * структури.
     */
    MusicTrack track1;
    track1.filename = "/mnt/sdcard/My Mucis/Nirvana - Smalls Like Teen Spiri.mp3";
    track1.title = "Smells like teen spiri";
    track1.band = "Nirvana";
    track1.album = "Nevermind";
    track1.duration = 5 * 60 + 3;

    /*
     * Сконструюємо список, котрий ми описали у файлі заголовку.
     */
    LChunk * list = new LChunk();
    list->value = 12;
    list->next = NULL;

    for (int i = 0; i < 15; push_back(list, i++));

    print(list);


    /*
     * А тут скористаємося списком, котрий для нас є підготовленим стандартною
     * бібліотекою.
     */
    std::list<double> dList;
    for (int i = 0; i < 15; push_back(dList, i * i++));
    print(dList);

    return 0;
};

int len(LChunk const * list) {
    int len = 0;
    for(;list; list = list->next, ++len);
    return len;
};

int len(std::list<double> const & list) {
    return list.size();
};

int push_back(LChunk * list, LChunk * element) {
    element->next = NULL;
    for(;list->next; list = list->next);
    list->next = element;
    return len(list);
};

int push_back(LChunk * list, double element) {
    LChunk * chunk = new LChunk();
    chunk->value = element;
    chunk->next = NULL;
    return push_back(list, chunk);
};

int push_back(std::list<double> & list, double element) {
    list.push_back(element);
    return list.size();
};

void print(LChunk const * list) {
    std::cout << "Printing LChunk list[" << list << "]:" << std::endl;
    for(;list; list = list->next) {
        std::cout << list->value << " ";
    };
    std::cout << std::endl;
};

void print(std::list<double> const & list) {
    std::cout << "Printing std::list list" << std::endl;
    for (std::list<double>::const_iterator i = list.begin(); i != list.end(); ++i) {
        std::cout << *i << " ";
    };
    std::cout << std::endl;
};
